import sys
import uproot
import uproot3

import pandas as pd # to store data as dataframe
import time # to measure time to analyse
import math # for mathematical functions such as square root
import numpy as np # # for numerical calculations such as histogramming
import matplotlib.pyplot as plt # for plotting
from matplotlib.ticker import AutoMinorLocator # for minor ticks
import tensorflow as tf
from tensorflow import keras
from tensorflow.random import set_seed

from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin, clone
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.ensemble import RandomForestClassifier

from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split

from sklearn.metrics import (
    precision_recall_curve,
    plot_precision_recall_curve,
    average_precision_score,
    roc_curve,
    auc,
    roc_auc_score,
    precision_recall_curve,
    classification_report
)
import compare_test_train

status = len(tf.config.experimental.list_physical_devices("GPU"))
print(status)
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

branches = ["mJJ", "pTJJ", "dEtaJJ","pTBal","dRB1Ph", "dRB2Ph", "cenPhJJ", "dPhiBBJJ", "dRB1J1", "cosThetaC", "nJets","pTBB", "pTPh"]

tree = "Nominal"
seed = 123456
set_seed(seed)

numEpochs = 10
batchSize = 256


# Data read from file.
signal = uproot3.open("/gpfs/slac/atlas/fs1/d/cagee/CxAODReaderCore/cxaodreader_vbfhbbph/run/346485ade_cgeeMaker.root")[tree]
print(signal.show())
df_signal = signal.pandas.df(branches)
print(df_signal)
#add additional kinematic cuts
df_signal_cut = df_signal[(df_signal.mJJ > 800000.) & (df_signal.pTBB > 60000.) & (df_signal.pTPh > 30000.)]
print("signal cut", df_signal_cut)

QCD_background = uproot3.open("/gpfs/slac/atlas/fs1/d/cagee/CxAODReaderCore/cxaodreader_vbfhbbph/run/344180ade_cgeeMaker.root")[tree]
df_background = QCD_background.pandas.df(branches)
print(df_background)
#add additional kinematic cuts
df_background_cut = df_background[(df_background.mJJ > 800000.) & (df_background.pTBB > 60000.) & (df_background.pTPh > 30000.)]
print("background cut", df_background_cut)

#shuffle background so not sorted by mc16ade
shuffleBackground = shuffle(df_background, random_state=seed)

# Signal and shuffle background data.
rawdata = pd.concat([df_signal, shuffleBackground])
X = rawdata.drop(["pTBB", "pTPh"], axis=1) #drop kinematic variables that were used for cuts but not training

# Labeling data with 1's and 0's to distinguish.(1/positve/signal and 0/negative/background)
# Truth Labels.
y = np.concatenate((np.ones(len(df_signal)), np.zeros(len(shuffleBackground))))
#print(y)

# This will split your data into train-test sets: 67%-33%. 
# It will also shuffle entries so you will not get the first 67% of X for training 
# and the last 33% for testing. 
# This is particularly important in cases where you load all signal events first 
# and then the background events.

# Here we split our data into two independent samples. 
# The split is to create a training and testing set. 
# The first will be used for classifier training and the second to evaluate its performance.

# make train and test sets
X_train,X_test, y_train,y_test = train_test_split(X, y, 
                                                  test_size=0.5, 
                                                  random_state=seed ) # set the random seed for reproducibility

#try to fix size and logit error
#y_train = np.asarray(y_train).astype('float32').reshape((-1,1))
#y_test = np.asarray(y_test).astype('float32').reshape((-1,1))

"""
# Shuffle full data and split into train/test and validation set.
X_dev, X_eval, y_dev, y_eval = train_test_split(
    X, y, test_size=0.001, random_state=seed, stratify=y
)

X_train, X_test, y_train, y_test = train_test_split(
    X_dev, y_dev, test_size=0.2, random_state=seed, stratify=y_dev
)
"""

#standard scalar to ensure that all numerical attributes are scaled to have a mean of 0 and standard deviation of 1 so ML model can converge

scaler = StandardScaler() #initialize standard scaler
#Fit only to training data
scaler.fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_scaled = scaler.transform(X)
X_test_scaled = scaler.transform(X_test)

#Neural Network with Keras
def build_model(n_hidden=10, n_neurons=128, learning_rate=1e-3): # function to build a neural network model
    # Build
    model = keras.models.Sequential() # initialise the model
    for layer in range(n_hidden): # loop over hidden layers
        model.add(keras.layers.Dense(n_neurons, activation="relu")) # original--add layer to your model
        #model.add(keras.layers.Dense(n_neurons, activation="tanh")) # add layer to your model
    #model.add(keras.layers.Dense(2, activation='sigmoid')) # add output layer--signomid for binary classification
    model.add(keras.layers.Dense(2, activation='softmax')) # original--add output layer--softmax for multi-classification
    # Compile
    optimizer = keras.optimizers.SGD(learning_rate=learning_rate) # original--define the optimizer
    #optimizer = keras.optimizers.Nadam(learning_rate=learning_rate)
    #model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy']) # compile your model
    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics=['accuracy']) # original--compile your model
    #model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy']) # compile your model
    return model

"""
#neural net
def build_model_nn(n_hidden=10, n_neurons=10, learning_rate=1e-3):
    # Build
    model = keras.models.Sequential()
    for layer in range(n_hidden):
        model.add(keras.layers.Dense(n_neurons, activation="relu"))
    model.add(keras.layers.Dense(4, activation='softmax'))
    # Compile
    optimizer = keras.optimizers.SGD(lr=learning_rate)
    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model
"""



#AMS score
def AMS(tpr, fpr, b_reg): # define function to calculate AMS
    return np.sqrt(2*((tpr+fpr+b_reg)*np.log(1+tpr/(fpr+b_reg))-tpr)) # equation for AMS

#keep some events for validation
X_valid_scaled, X_train_nn_scaled = X_train_scaled[:200000], X_train_scaled[200000:] # first 100 events for validation
y_valid, y_train_nn = y_train[:200000], y_train[200000:] # first 100 events for validation

#train network
tf_clf = keras.wrappers.scikit_learn.KerasClassifier(build_model) # call the build_model function defined earlier
tf_clf.fit(X_train_nn_scaled, y_train_nn, validation_data=(X_valid_scaled, y_valid), epochs=numEpochs, batch_size=batchSize) # fit your neural network

#predicted y values for Tensorflow NN
y_pred_tf = tf_clf.predict(X_test_scaled) # make predictions on the test data

# See how well the classifier does
print("accuracy_score(y_test, y_pred_tf)")
print(accuracy_score(y_test, y_pred_tf))
print(y_test)
print(y_pred_tf)

#overfitting check
#compare_train_test(tf_clf, X_train, y_train, X_test, y_test, 'Tensorflow neural net output')


#Print the same classification report as the Model Comparison lesson for your TensorFlow neural network.
# TensorFlow Neural Network Report
print (classification_report(y_test, y_pred_tf,
                            target_names=["QCD_background", "signal"]))


#decisions of TensorFlow neural network
#decisions_tf = (tf_clf.predict(X_test_scaled)>0.5).astype("int32") # get the decisions of the TensorFlow neural network  
decisions_tf = tf_clf.predict_proba(X_test_scaled)[:,1] #--works with sigmoid
#decisions_tf = np.argmax(tf_clf.predict(X_test_scaled), axis=-1)
#d1 =  (tf_clf.predict(X_test_scaled)>0.5).astype("int32")  # signal
#d2 =  (tf_clf.predict(X_test_scaled)<0.5).astype("int32")  # background
d1 = tf_clf.predict_proba(X_scaled[y<0.5])[:, 1] # background
d2 = tf_clf.predict_proba(X_scaled[y>0.5])[:, 1] # signal
print("d1 & d2 defined")
print(d1)
print(d2)


#decisions_tf = np.argmax(tf_clf.predict(X_test_scaled), axis=-1)
print("decisions_tf",decisions_tf)


#False positive rate and true positive rate
fpr_tf, tpr_tf, thresholds_tf = roc_curve(y_test, decisions_tf) # get FPRs, TPRs and thresholds for TensorFlow neural network
precision_tf, recall_tf, thresholds_pr_tf = precision_recall_curve(y_test, decisions_tf)
print("Sig Eff/Bkg Eff AUC", auc(tpr_tf, 1.-fpr_tf))

print("fpr_tf",fpr_tf)
print("tpr_tf",tpr_tf)
print("thresholds_tf", thresholds_tf)


#pytorch nn
pytorch = 0
if (pytorch == 1):
    NN_clf = keras.wrappers.scikit_learn.KerasClassifier(build_model)
    NN_clf.fit(X_train_scaled, y_train, validation_data=(X_valid_scaled, y_valid), epochs=numEpochs, batch_size=batchSize)
    y_pred_NN = NN_clf.predict(X_test_scaled)

    # See how well the classifier does
    print("PyTorch NN accuracy score", accuracy_score(y_test, y_pred_NN))

    decisions_nn = NN_clf.predict_proba(X_test_scaled)[:,1]
    fpr_nn, tpr_nn, thresholds_nn = roc_curve(y_test, decisions_nn)
    



plt.figure()
plt.hist(decisions_tf[y_test==1], color='b', histtype='step', bins=50, label='Higgs Events')
plt.hist(decisions_tf[y_test==0], color='g', histtype='step', bins=50, label='Background Events')
plt.xlabel('Threshhold')
plt.ylabel('Number of Events')
#plt.semilogy()
plt.legend()
plt.savefig('sig_bkg_test.png')
plt.show()

randomForest = 0
if (randomForest == 1):
    print("start random forest")
    #Random Forest
    RF_clf = RandomForestClassifier(criterion='gini', max_depth=8, n_estimators=30)
    print("defined Random Forest")
    RF_clf.fit(X_train_scaled, y_train)
    print("Fit Random Forest")
    y_pred_RF = RF_clf.predict(X_test_scaled)

    # See how well the classifier does
    print(accuracy_score(y_test, y_pred_RF))

    # Random Forest Report
    print (classification_report(y_test, y_pred_RF,
                                 target_names=["background", "signal"]))

    decisions_rf = RF_clf.predict_proba(X_test)[:,1]
    fpr_rf, tpr_rf, thresholds_rf = roc_curve(y_test, decisions_rf)
    


#Make ROC curve plot
#plt.plot(fpr_rf, tpr_rf, label='Random Forest') # plot random forest ROC
plt.plot(fpr_tf, tpr_tf, linestyle='dashdot', label='TensorFlow Neural Network') # plot TensorFlow neural network ROC
#plt.plot(fpr_nn,tpr_nn, color='r', ls='--', label='PyTorch Neural Network') #plot PyTorch neural network ROC
#plt.plot([0, 1], [0, 1], linestyle='dotted', color='grey', label='Luck') # plot diagonal line to indicate luck
plt.xlabel('False Positive Rate') # x-axis label
plt.ylabel('True Positive Rate') # y-axis label
plt.grid() # add a grid to the plot
plt.legend() # add a legend
plt.show()

#precision-recall used when there is a large class imbalance
average_precision = average_precision_score(y_test, y_pred_tf)

print('Average precision-recall score: {0:0.2f}'.format(
      average_precision))

#disp_pr_tf = plot_precision_recall_curve(tf_clf, X_test_scaled, y_test)
#disp.ax_.set_title('2-class Precision-Recall curve: '
#                   'AP={0:0.2f}'.format(average_precision))

#Signal Efficiency and Background Rejection plot
plt.plot(tpr_tf, 1.-fpr_tf, linestyle='dashdot', label="TensorFlow NN Signal Efficiency/Background Rejection curve")
plt.xlabel('Signal Efficiency')
plt.ylabel('Background Rejection')
plt.grid()
plt.legend()
plt.savefig('roc_test.png')
plt.show()

"""
plt.plot(recall_tf, precision_tf, linestyle='dashdot', label="TensorFlow NN precision recall curve")
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.grid()
plt.legend()
plt.show()
"""

#AMS score
ams_tf = AMS(tpr_tf, fpr_tf, 0.001) # original = .001 get AMS for TensorFlow neural network
#ams_nn = AMS(tpr_nn, fpr_nn, 0.001) # get AMS for TensorFlow neural network
#ams_rf = AMS(tpr_rf, fpr_rf, 0.001) # get AMS for TensorFlow neural network

#plt.plot(thresholds_rf, ams_rf, label='Random Forest') # plot random forest AMS
#plt.plot(thresholds_nn, ams_nn, linestyle='dashed', label='PyTorch Neural Network') # plot PyTorch neural network AMS
plt.plot(thresholds_tf, ams_tf, linestyle='dashdot', label='TensorFlow Neural Network') # plot TensorFlow neural network AMS
plt.xlabel('Threshold') # x-axis label
plt.ylabel('AMS') # y-axis label
plt.title('AMS with $b_r=0.001$') # add plot title
plt.legend() # add a legend
plt.show()

#Overtraining check
"""
def compare_train_test(tf_clf, X_train_scaled, y_train, X_test_scaled, y_test):
    bins = 30
    decisions = []
    for X_scaled,y in ((X_train_scaled, y_train), (X_test_scaled, y_test)):
        d1 = tf_clf.predict_proba(X_scaled[y<0.5])[:, 1] # background
        d2 = tf_clf.predict_proba(X_scaled[y>0.5])[:, 1] # signal
        decisions += [d1, d2]
        
    low = min(np.min(d) for d in decisions)
    high = max(np.max(d) for d in decisions)
    low_high = (low,high)
    
    plt.hist(decisions[0],
             color='r', alpha=0.5, range=low_high, bins=bins,
             histtype='stepfilled', density=True,
             label='S (train)')
    plt.hist(decisions[1],
             color='b', alpha=0.5, range=low_high, bins=bins,
             histtype='stepfilled', density=True,
             label='B (train)')

    hist, bins = np.histogram(decisions[2],
                              bins=bins, range=low_high, density=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale
    
    width = (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt='o', c='r', label='S (test)')
    
    hist, bins = np.histogram(decisions[3],
                              bins=bins, range=low_high, density=True)
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    plt.errorbar(center, hist, yerr=err, fmt='o', c='b', label='B (test)')

    plt.xlabel("NN output")
    plt.ylabel("Arbitrary units")
    plt.legend(loc='best')
    plt.savefig('comp_test_train.png')
    plt.show()
"""

compare_train_test(tf_clf, X_train_scaled, y_train, X_test_scaled, y_test)
